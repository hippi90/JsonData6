package org.labjournal.utils.jsondata;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.labjournal.utils.jsondata.internal.JsonDataList;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class JsonData {
	private JsonNode root;
	private JsonNode parent;
	private String name;
	private ObjectMapper mapper = new ObjectMapper();
	
	public JsonData(String json) {
		if (json == null || "".equals(json)) {
			root = mapper.createObjectNode();
			parent = null;
			name = null;
			return;
		}
		
		try {
			root = mapper.readTree(json);
			parent = null;
			name = null;
		}
		catch (IOException e) {
			e.printStackTrace();			
			throw new ParsingException(e.getMessage());
		}
	}
	
	private JsonData(JsonNode node, JsonNode parent, String name)
	{
		this.root = node;
		this.parent = parent;
		this.name = name;
	}
	
	public JsonData()
	{
		root = mapper.createObjectNode();
		parent = null;
		name = null;
	}
	
	public JsonData(JsonNode root) {
		this.root = root;
		parent = null;
		name = null;
	}
	
	/*
	 * Getter and setter for Boolean
	 */
	public Boolean getBoolean(String path)
	{		
		JsonNode node = getNodeByPath(path);
		
		if (!node.isMissingNode()) {
			return node.asBoolean();
		} else {
			return null;
		}
	}
	
	public void setBoolean(String path, boolean value)
	{
		String[] pathAndvalue = getPathAndvalue(path);
		JsonNode node = getNodeByPath(pathAndvalue[0]);
		
		if (node.isMissingNode()) {
			throw new NoSuchPathException("There is no such path in object: "+path);
		}
		if (!node.getNodeType().equals(JsonNodeType.OBJECT)) {
			throw new NoSuchPathException("There is not object node in path: "+path);
		}
		((ObjectNode)node).put(pathAndvalue[1], value);
	}
	
	/*
	 * Getter and setter for Integer
	 */
	public Integer getInteger(String path)
	{		
		JsonNode node = getNodeByPath(path);
		
		if (!node.isMissingNode() && node.canConvertToInt()) {
			return node.asInt();
		} else {
			return null;
		}
	}
	
	public void setInteger(String path, int value)
	{
		String[] pathAndvalue = getPathAndvalue(path);
		JsonNode node = getNodeByPath(pathAndvalue[0]);
		
		if (node.isMissingNode()) {
			throw new NoSuchPathException("There is no such path in object: "+path);
		}
		if (!node.getNodeType().equals(JsonNodeType.OBJECT)) {
			throw new NoSuchPathException("There is not object node in path: "+path);
		}
		((ObjectNode)node).put(pathAndvalue[1], value);
	}
	
	/*
	 * Getter and setter for String
	 */
	public Long getLong(String path)
	{
		JsonNode node = getNodeByPath(path);
		
		if (!node.isMissingNode() && node.canConvertToLong()) {
			return node.asLong();
		} else {
			return null;
		}
	}
	
	public void setLong(String path, Long value)
	{
		String[] pathAndvalue = getPathAndvalue(path);
		JsonNode node = getNodeByPath(pathAndvalue[0]);
		
		if (node.isMissingNode()) {
			throw new NoSuchPathException("There is no such path in object: "+path);
		}
		if (!node.getNodeType().equals(JsonNodeType.OBJECT)) {
			throw new NoSuchPathException("There is not object node in path: "+path);
		}
		((ObjectNode)node).put(pathAndvalue[1], value);
	}
	
	/*
	 * Getter and setter for String
	 */
	public Double getDouble(String path)
	{
		JsonNode node = getNodeByPath(path);
		
		if (!node.isMissingNode()) {
			return node.asDouble();
		} else {
			return null;
		}
	}
	
	public void setDouble(String path, Double value)
	{
		String[] pathAndvalue = getPathAndvalue(path);
		JsonNode node = getNodeByPath(pathAndvalue[0]);
		
		if (node.isMissingNode()) {
			throw new NoSuchPathException("There is no such path in object: "+path);
		}
		if (!node.getNodeType().equals(JsonNodeType.OBJECT)) {
			throw new NoSuchPathException("There is not object node in path: "+path);
		}
		((ObjectNode)node).put(pathAndvalue[1], value);
	}
	
	/*
	 * Getter and setter for String
	 */
	public String getString(String path)
	{
		JsonNode node = getNodeByPath(path);
		
		if (!node.isMissingNode()) {
			return node.asText();
		} else {
			return null;
		}
	}
	
	public void setString(String path, String value)
	{
		String[] pathAndvalue = getPathAndvalue(path);
		JsonNode node = getNodeByPath(pathAndvalue[0]);
		
		if (node.isMissingNode()) {
			throw new NoSuchPathException("There is no such path in object: "+path);
		}
		if (!node.getNodeType().equals(JsonNodeType.OBJECT)) {
			throw new NoSuchPathException("There is not object node in path: "+path);
		}
		((ObjectNode)node).put(pathAndvalue[1], value);
	}
	
	/*	
	 * Getter and setter for JsonData
	 */
	public Date getDate(String path)
	{
		JsonNode node = getNodeByPath(path);
		if (!node.isMissingNode()) {
			return new Date(node.asLong());
		} else {
			return null;
		}
	}
		
	public void setDate(String path, Date value)
	{		
		String[] pathAndvalue = getPathAndvalue(path);
		JsonNode node = getNodeByPath(pathAndvalue[0]);
		
		if (node.isMissingNode()) {
			throw new NoSuchPathException("There is no such path in object: "+path);
		}
		if (!node.getNodeType().equals(JsonNodeType.OBJECT)) {
			throw new NoSuchPathException("There is not object node in path: "+path);
		}
		Long time;
		if (value != null) {
			time = value.getTime();
		}
		else {
			time = null;
		}
		
		((ObjectNode)node).put(pathAndvalue[1], time);
	}
	
	/*	
	 * Getter and setter for JsonData
	 */
	public JsonData getJsonData(String path)
	{
		String[] res = getPathAndvalue(path);
		JsonNode node = getNodeByPath(path);		
		JsonNode parentNode = getNodeByPath(res[0]);
		if (!node.isMissingNode()) {
			return new JsonData(node, parentNode, res[1]);
		} else {
			return null;
		}
	}
		
	public void setJsonData(String path, JsonData value)
	{		
		String[] pathAndvalue = getPathAndvalue(path);
		JsonNode node = getNodeByPath(pathAndvalue[0]);
		
		if (node.isMissingNode()) {
			throw new NoSuchPathException("There is no such path in object: "+path);
		}
		if (!node.getNodeType().equals(JsonNodeType.OBJECT)) {
			throw new NoSuchPathException("There is not object node in path: "+path);
		}
		if (!"".equals(path)) {
			((ObjectNode) node).set(pathAndvalue[1], value.root);
		}
		else {
			if (parent != null) {
				((ObjectNode) parent).set(name, value.root);
				root = parent.path(name);
			}
			else {
				root = value.root;
			}
		}
	}

	public List<JsonData> getList(String path)
	{
		JsonNode node = getNodeByPath(path);		
		if (!node.isMissingNode()) {
			if (node.isArray()) {
				return new JsonDataList((ArrayNode) node);
			} else {
				throw new NoSuchPathException("This node is not a list! "+path);
			}
		} else {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public void setList(String path, List<?> value)
	{
		ArrayNode array;
		
		if (value == null || value.size() == 0) {
			array = mapper.createArrayNode();
		} else {
			if (value instanceof JsonDataList) {
				array = ((JsonDataList)value).getRoot();
			} else if (value.get(0) instanceof JsonData) {
				array = mapper.createArrayNode();
				for (JsonData item: (List<JsonData>)value) {
					array.add(item.root);
				}
			} else {			
				try {
					array = mapper.valueToTree(value);
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
					throw new IllegalArgumentException("Can't convert list! " + e.getMessage());
				}
			}
		}		
		
		if (array.isArray()) {
			String[] pathAndvalue = getPathAndvalue(path);
			JsonNode node = getNodeByPath(pathAndvalue[0]);
			
			if (node.isMissingNode()) {
				throw new NoSuchPathException("There is no such path in object: "+path);
			}
			if (!node.getNodeType().equals(JsonNodeType.OBJECT)) {
				throw new NoSuchPathException("There is not object node in path: "+path);
			}
			if (!"".equals(path)) {
				((ObjectNode) node).set(pathAndvalue[1], array);
			}
			else {
				if (parent != null) {
					((ObjectNode) parent).set(name, array);
					root = parent.path(name);
				}
				else {
					root = array;
				}
			}
		}
	}
	
	public JsonNode getRoot() {
		return root;
	}
	
	public void setRoot(JsonNode jsonNode) {
		this.root = jsonNode;		
	}
	
	public void unset(String path) {
		if (path == null || "".equals(path)) {
			return;
		}
		
		String[] pathParts = getPathAndvalue(path);
		ObjectNode parent = (ObjectNode) getNodeByPath(pathParts[0]);
		if (parent != null) {
			parent.remove(pathParts[1]);
		}
	}
	
	public boolean exists(String path)
	{
		return !getNodeByPath(path).isMissingNode();
	}
	
	public String toString()
	{
		try {
			return mapper.writeValueAsString(root);
		}
		catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	private JsonNode getNodeByPath(String path)
	{
		if (path == null || path.equals("") || path.equals("/")) {
			return root;
		}
		
		String[] elems = path.split("/");		
		JsonNode node = root;
		for (String elem: elems) {
			node = node.path(elem);
			if (node.isMissingNode()) {
				break;
			}
		}
		
		return node;
	}
	
	private String[] getPathAndvalue(String path)
	{
		if (path == null || "".equals(path)) {
			return new String[] {null, ""};
		}			
		
		String[] result = new String[2];
		StringBuilder sb = new StringBuilder(path);
		if (sb.charAt(0) == '/') {
			sb.deleteCharAt(0);
		}
		if (sb.charAt(sb.length()-1) == '/') {
			sb.deleteCharAt(sb.length()-1);
		}

		int n = sb.lastIndexOf("/");
		if (n != -1) {
			result[0] = sb.toString().substring(0, n);
			result[1] = sb.toString().substring(n+1);
		} else {
			result[0] = null;
			result[1] = sb.toString();
		}
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (root == null) {
			return obj == null;
		}
		
		if (obj == null) {
			return root.getNodeType().equals(JsonNodeType.NULL);
		}
		
		if (obj instanceof JsonData) {
			return root.equals(((JsonData)obj).root);
		}
		return root.equals(obj);
	}
}
