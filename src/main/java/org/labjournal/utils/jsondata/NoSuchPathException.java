package org.labjournal.utils.jsondata;

public class NoSuchPathException extends IllegalArgumentException {

	public NoSuchPathException(String message) {
		super(message);
	}

	private static final long serialVersionUID = 1L;

}
