package org.labjournal.utils.jsondata.internal;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;

import org.labjournal.utils.jsondata.JsonData;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;

public class JsonDataList implements List<JsonData> {

	private ArrayNode root;
	
	public JsonDataList(ArrayNode root) {
		this.root = root;
	}
	
	public ArrayNode getRoot() {
		return root;
	}
	
	@Override
	public boolean add(JsonData elem) {
		root.add(elem.getRoot());
		return true;
	}

	@Override
	public void add(int index, JsonData elem) {
		root.insert(index, elem.getRoot());		
	}

	@Override
	public boolean addAll(Collection<? extends JsonData> elems) {
		for (JsonData elem: elems) {
			root.add(elem!=null?elem.getRoot():null);
		}
		return true;
	}

	@Override
	public boolean addAll(int index, Collection<? extends JsonData> elems) {
		for (JsonData elem: elems) {
			root.insert(index++, elem.getRoot());
		}
		return true;
	}

	@Override
	public void clear() {
		root.removeAll();		
	}

	@Override
	public boolean contains(Object o) {
		if (indexOf(o) != -1) {
			return true;
		}
		return false;
	}

	@Override
	public boolean containsAll(Collection<?> elems) {
		for (Object o: elems) {
			if (!contains(o)) {
				return false;
			}
		}
		
		return true;
	}

	@Override
	public JsonData get(int index) {
		JsonData elem = new JsonData();
		elem.setRoot(root.get(index));
		return elem;
	}

	@Override
	public int indexOf(Object o) {
		if (o == null || ((JsonData)o).getRoot() == null) {
			for (int i = 0; i < root.size(); i++) {
				if (root.get(i) == null || root.get(i).getNodeType().equals(JsonNodeType.NULL)) {
					return i;
				}
			}
			return -1;
		}
		
		
		for (int i = 0; i < root.size(); i++) {
			if ((((JsonData)o).getRoot()).equals(root.get(i))) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public boolean isEmpty() {
		return root.size() == 0;
	}

	@Override
	public Iterator<JsonData> iterator() {
		return new JsonDataListIterator(this);
	}
	
	class JsonDataListIterator implements Iterator<JsonData> {
		private JsonDataList root;
		int cursor = 0;
		
		public JsonDataListIterator(JsonDataList root) {
			this.root = root;
		}
		
		@Override
		public boolean hasNext() {
			return cursor < root.size();
		}

		@Override
		public JsonData next() {
			return root.get(cursor++);
		}
		
	}

	@Override
	public int lastIndexOf(Object o) {
		if (o == null || ((JsonData)o).getRoot() == null) {
			for (int i = root.size()-1; i > 0; i--) {
				if (root.get(i) == null || root.get(i).getNodeType().equals(JsonNodeType.NULL)) {
					return i;
				}
			}
			return -1;
		}		
		
		for (int i = root.size()-1; i > 0; i--) {
			if ((((JsonData)o).getRoot()).equals(root.get(i))) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public ListIterator<JsonData> listIterator() {
		return new JsonDataListListIterator(this);
	}

	@Override
	public ListIterator<JsonData> listIterator(int arg0) {
		return new JsonDataListListIterator(this, arg0);
	}
	
	class JsonDataListListIterator implements ListIterator<JsonData> {
		private JsonDataList root;
		int cursor;
		int initCursor;
		
		public JsonDataListListIterator(JsonDataList root) {
			this.root = root;
			this.cursor = 0;
			this.initCursor = this.cursor;
		}
		
		public JsonDataListListIterator(JsonDataList root, int cursor) {
			this.root = root;
			this.cursor = cursor;
			this.initCursor = this.cursor;
		}
		
		@Override
		public void add(JsonData e) {
			root.add(e);
			cursor++;
		}

		@Override
		public boolean hasNext() {
			return cursor < root.size();
		}

		@Override
		public boolean hasPrevious() {
			return cursor >= initCursor;
		}

		@Override
		public JsonData next() {
			return root.get(cursor++);
		}

		@Override
		public int nextIndex() {
			return cursor;
		}

		@Override
		public JsonData previous() {
			if (cursor == 0) {
				throw new NoSuchElementException();
			}
			return root.get(--cursor);
		}

		@Override
		public int previousIndex() {
			return cursor - 1;
		}

		@Override
		public void remove() {
			root.remove(cursor);			
		}

		@Override
		public void set(JsonData e) {
			root.set(cursor, e);			
		}
		
	}

	@Override
	public boolean remove(Object o) {
		int index = indexOf(o);
		if (index != -1) {
			remove(index);
			return true;
		}
		return false;
	}

	@Override
	public JsonData remove(int index) {
				
		JsonNode oldValue = root.get(index);
		root.remove(index);
		
		return new JsonData(oldValue);
		
	}

	@Override
	public boolean removeAll(Collection<?> elems) {
		boolean res = false;
		for (Object o: elems) {
			while(remove(o)) res |= true;
		}
		
		return res;
	}

	@Override
	public boolean retainAll(Collection<?> elems) {
		boolean res = false;
		for (Iterator<JsonNode> it = root.elements(); it.hasNext();) {
			if (!elems.contains(new JsonData(it.next()))) {
				it.remove();
				res = true;
			}
		}
		
		return res;
	}

	@Override
	public JsonData set(int index, JsonData elem) {
		JsonData oldValue = new JsonData(root.get(index));
		add(index, elem);
		return oldValue;
	}

	@Override
	public int size() {
		return root.size();
	}

	@Override
	public List<JsonData> subList(int arg0, int arg1) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Object[] toArray() {
		throw new UnsupportedOperationException();
	}

	@Override
	public <T> T[] toArray(T[] arg0) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public String toString() {
		JsonData data = new JsonData();
		data.setRoot(root);
		return data.toString();
		
	}

}


