package org.labjournal.utils.jsondata;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class JsonDataTest {

	@Test
	@Parameters
	public void testGetBooleanValid(String json, String path, boolean value) {
		JsonData data = new JsonData(json);
		assertEquals(value, data.getBoolean(path));
	}
	
	@Test
	@Parameters
	public void testGetBooleanInvalid(String json, String path) {
		JsonData data = new JsonData(json);
		assertNull(data.getBoolean(path));
	}

	@Test
	@Parameters
	public void testSetBooleanValid(String json, String path, boolean value) {
		JsonData data = new JsonData(json);
		data.setBoolean(path, value);
		assertEquals(value, data.getBoolean(path));
	}
	
	@Test(expected=NoSuchPathException.class)
	@Parameters
	public void testSetBooleanInvalid(String json, String path, boolean value) {
		JsonData data = new JsonData(json);
		data.setBoolean(path, value);
		assertEquals(value, data.getBoolean(path));
	}

	@Test
	@Parameters
	public void testGetJsonDataValid(String json, String path, String value) {
		JsonData root = new JsonData(json);
				
		assertEquals(value, root.getJsonData(path).getString("name"));
	}
	
	@Test
	@Parameters
	public void testSetJsonDataValid(String json, String path, String data, String value) {
		JsonData root = new JsonData(json);
		JsonData dataNode = new JsonData(data);
		
		root.setJsonData(path, dataNode);
		assertEquals(value, root.getJsonData(path).getString("name"));
	}
	
	@Test
	@Parameters
	public void testGetPathAndvalue(String wholePath, String path, String value)
	{
		Method method = null;
		try {
			method = JsonData.class.getDeclaredMethod("getPathAndvalue", String.class);
			method.setAccessible(true);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		JsonData jsonData = new JsonData();
		
		String[] result = null;
		try {
			result = (String[]) method.invoke(jsonData, wholePath);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		
		assertNotNull(result);
		assertEquals(path, result[0]);
		assertEquals(value, result[1]);
	}
	
	@Test
	public void testGetList()
	{
		String json = "{\"lst\": [1, 2, 3, 4]}";
		JsonData data = new JsonData(json);
		List<JsonData> list  = data.getList("lst");
		
		assertEquals(4, list.size());
	}
	
	@Test
	public void testSetList()
	{
		String json = "{\"lst\": 1}";
		JsonData data = new JsonData(json);
		List<JsonData> list  = Arrays.asList(new JsonData(json));
		
		data.setList("newlst", list);
		assertEquals(1, data.getList("newlst").size());
	}
	
	@Test
	public void testExists()
	{
		String json = "{\"lst\": 1}";
		JsonData data = new JsonData(json);
		
		assertTrue(data.exists("lst"));
		assertFalse(data.exists("test"));
	}
	
	@Test
	public void testUnset()
	{
		String json = "{\"first\": 1, \"second\": 2, \"third\": {\"first\": 1}}";
		JsonData data = new JsonData(json);
		data.unset("second");
		assertFalse(data.exists("second"));
		data.unset("third/first");
		assertFalse(data.exists("third/first"));
	}
	
	//==== Method params ====
	@SuppressWarnings("unused")
	private Object[] parametersForTestGetBooleanValid()
	{
		return new Object[] {
			new Object[] {"{\"name\":\"test\",\"id\":1,\"data\":{\"name\":\"test2\",\"active\":false,\"data\":{\"name\":\"test2\",\"active\":true}}}", "data/active", false},
			new Object[] {"{\"name\":\"test\",\"id\":1,\"data\":{\"name\":\"test2\",\"active\":false,\"data\":{\"name\":\"test2\",\"active\":true}}}", "data/data/active", true}
		};
	}
	
	@SuppressWarnings("unused")
	private Object[] parametersForTestGetBooleanInvalid()
	{
		return new Object[] {
			new Object[] {"{\"name\":\"test\",\"id\":1,\"data\":{\"name\":\"test2\",\"active\":false,\"data\":{\"name\":\"test2\",\"active\":true}}}", "data/active/asdasdasda"},
			new Object[] {"{\"name\":\"test\",\"id\":1,\"data\":{\"name\":\"test2\",\"active\":false,\"data\":{\"name\":\"test2\",\"active\":true}}}", "/data/data/active/asdasasdasd/"}
		};
	}
	
	@SuppressWarnings("unused")
	private Object[] parametersForTestSetBooleanValid()
	{
		return new Object[] {
			new Object[] {"{\"name\":\"test\",\"id\":1,\"data\":{\"name\":\"test2\",\"active\":false,\"data\":{\"name\":\"test2\",\"active\":true}}}", "data/active", true},
			new Object[] {"{\"name\":\"test\",\"id\":1,\"data\":{\"name\":\"test2\",\"active\":false,\"data\":{\"name\":\"test2\",\"active\":true}}}", "data/data/active", false}
		};
	}
	
	@SuppressWarnings("unused")
	private Object[] parametersForTestSetBooleanInvalid()
	{
		return new Object[] {
			new Object[] {"{\"name\":\"test\",\"id\":1,\"data\":{\"name\":\"test2\",\"active\":false,\"data\":{\"name\":\"test2\",\"active\":true}}}", "data/active/sadfsdfsfsf", true},
			new Object[] {"{\"name\":\"test\",\"id\":1,\"data\":{\"name\":\"test2\",\"active\":false,\"data\":{\"name\":\"test2\",\"active\":true}}}", "data/data/sdfsfsf/sfsfsfsfsf", false}
		};
	}
	
	@SuppressWarnings("unused")
	private Object[] parametersForTestSetJsonDataValid()
	{
		return new Object[] {
			new Object[] {"{\"name\":\"test\",\"id\":1,\"data\":{\"name\":\"test2\",\"active\":false,\"data\":{\"name\":\"test2\",\"active\":true}}}", "data/test1", "{\"name\":\"test1\",\"active\":true}", "test1"},
			new Object[] {"{\"name\":\"test\",\"id\":1,\"data\":{\"name\":\"test2\",\"active\":false,\"data\":{\"name\":\"test2\",\"active\":true}}}", "data/data/test2", "{\"name\":\"test2\",\"active\":true}", "test2"}
		};
	}
	
	@SuppressWarnings("unused")
	private Object[] parametersForTestGetJsonDataValid()
	{
		return new Object[] {
			new Object[] {"{\"name\":\"test\",\"id\":1,\"data\":{\"name\":\"test1\",\"active\":false,\"data\":{\"name\":\"test2\",\"active\":true}}}", "data", "test1"},
			new Object[] {"{\"name\":\"test\",\"id\":1,\"data\":{\"name\":\"test1\",\"active\":false,\"data\":{\"name\":\"test2\",\"active\":true}}}", "data/data/", "test2"}
		};
	}
	
	@SuppressWarnings("unused")
	private Object[] parametersForTestGetPathAndvalue()
	{
		return new Object[]
		{
			new Object[] {"data/test", "data", "test"},
			new Object[] {"", null, ""},
			new Object[] {"data", null, "data"},
			new Object[] {"data/", null, "data"},
			new Object[] {"data/data/data/data/data", "data/data/data/data", "data"}
		};
	}
}
