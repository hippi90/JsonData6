package org.labjournal.utils.jsondata.internal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.labjournal.utils.jsondata.JsonData;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class JsonDataListTest {
	private static JsonDataList jsonDataList;
	private static ObjectMapper objectMapper = new ObjectMapper();
	
	@Before
	public void onBeforeTest() {
		ArrayNode root = objectMapper.createArrayNode();
		root.add(0);
		root.add(1);
		root.add(2);
		root.add((JsonNode)null);
		root.add(3);
		jsonDataList = new JsonDataList(root);
		jsonDataList.get(0);
	}
	
	@Test
	@Parameters(method="simpleStringData")
	public void testAddJsonData(String num) {
		JsonData data = new JsonData(num);
		
		jsonDataList.add(data);
		assertEquals(6, jsonDataList.size());
		assertTrue(jsonDataList.contains(data));
	}

	@Test
	@Parameters(method="simpleStringData")
	public void testAddIntJsonData(String num) {
		JsonData data = new JsonData(num);
		
		jsonDataList.add(2, data);
		assertEquals(6, jsonDataList.size());
		assertTrue(jsonDataList.contains(data));
		assertEquals(2, jsonDataList.indexOf(data));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testAddAllCollectionOfQextendsJsonData() {
		List<List<JsonData>> data = Arrays.asList(
				Arrays.asList(new JsonData("4"), new JsonData(""), null),
				new ArrayList<JsonData>());
		
		int size = jsonDataList.size();
		for (List<JsonData> collection: data) {
			jsonDataList.addAll(collection);
			size+=collection.size();
			assertEquals(size, jsonDataList.size());
		}		
	}

	@Test
	public void testClear() {
		jsonDataList.clear();
		assertEquals(0, jsonDataList.size());
	}

	@Test
	public void testContains() {
		assertTrue(jsonDataList.contains(new JsonData("0")));
		assertTrue(jsonDataList.contains(null));
		
		JsonData data = new JsonData();
		data.setRoot(null);
		assertTrue(jsonDataList.contains(data));
		assertFalse(jsonDataList.contains(new JsonData()));
		assertFalse(jsonDataList.contains(new JsonData("-1")));
	}

	@Test
	public void testContainsAll() {
		assertTrue(jsonDataList.containsAll(Arrays.asList(new JsonData("0"), new JsonData("1"), null)));
		assertFalse(jsonDataList.containsAll(Arrays.asList(new JsonData("0"), new JsonData("-1"), null)));
	}

	@Test
	public void testGet() {
		assertTrue(jsonDataList.get(0).equals(new JsonData("0")));
		assertTrue(jsonDataList.get(3).equals(null));
	}

	@Test
	public void testIndexOf() {
		assertTrue(jsonDataList.indexOf(new JsonData("0")) == 0);
		assertTrue(jsonDataList.indexOf(null) == 3);
		assertTrue(jsonDataList.indexOf(new JsonData("-1")) == -1);
	}

	@Test
	public void testIsEmpty() {
		assertFalse(jsonDataList.isEmpty());
		jsonDataList.clear();
		assertTrue(jsonDataList.isEmpty());
	}

	@Test
	public void testIterator() {
		Iterator<JsonData> it = jsonDataList.iterator();
		assertNotNull(it);
		assertTrue(it.hasNext());
		assertTrue(it.next().equals(new JsonData("0")));
	}

	@Test
	public void testLastIndexOf() {
		jsonDataList.addAll(Arrays.asList(new JsonData("4"), new JsonData("4"), null, new JsonData("5")));
		assertTrue(jsonDataList.lastIndexOf(new JsonData("4")) == 6);
		assertTrue(jsonDataList.lastIndexOf(null) == 7);
	}

	@Test
	public void testListIterator() {
		ListIterator<JsonData> it = jsonDataList.listIterator();
		assertNotNull(it);
		assertTrue(it.hasNext());
		assertTrue(it.next().equals(new JsonData("0")));
		assertTrue(it.hasPrevious());
		assertTrue(it.previous().equals(new JsonData("0")));
	}

	@Test
	public void testListIteratorInt() {
		ListIterator<JsonData> it = jsonDataList.listIterator(1);
		assertNotNull(it);
		assertTrue(it.hasNext());
		assertTrue(it.next().equals(new JsonData("1")));
		assertTrue(it.hasPrevious());
		assertTrue(it.previous().equals(new JsonData("1")));
	}

	@Test
	public void testRemoveObject() {
		jsonDataList.remove(new JsonData("0"));
		assertTrue(jsonDataList.size() == 4);
		assertFalse(jsonDataList.contains(new JsonData("0")));
	}

	@Test
	public void testRemoveInt() {
		JsonData data = jsonDataList.get(1);
		jsonDataList.remove(1);
		assertFalse(jsonDataList.contains(data));
	}

	@Test
	public void testRemoveAll() {
		jsonDataList.addAll(Arrays.asList(new JsonData("4"), new JsonData("4"), null, new JsonData("5")));
		jsonDataList.removeAll(Arrays.asList(new JsonData("4"), null, new JsonData("5")));
		assertFalse(jsonDataList.contains(new JsonData("4")));
		
		assertFalse(jsonDataList.removeAll(Arrays.asList(new JsonData("4"), null, new JsonData("5"))));
	}

	@Test
	public void testRetainAll() {
		jsonDataList.retainAll(Arrays.asList(new JsonData("0"), null, new JsonData("1")));
		assertTrue(jsonDataList.contains(new JsonData("0")));
		assertTrue(jsonDataList.contains(new JsonData("1")));
		assertTrue(jsonDataList.contains(null));
		assertFalse(jsonDataList.contains(new JsonData("2")));
		assertFalse(jsonDataList.contains(new JsonData("3")));
	}

	@Test
	public void testSet() {
		jsonDataList.set(0, new JsonData("12"));
		assertTrue(jsonDataList.contains(new JsonData("12")));
		assertEquals(new JsonData("12"), jsonDataList.get(0));
	}

//	@Test
//	public void testSubList() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testToArray() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testToArrayTArray() {
//		fail("Not yet implemented");
//	}
	
	String[] simpleStringData() {
		return new String[] {
			"", "4", null 
		};
	}
}
